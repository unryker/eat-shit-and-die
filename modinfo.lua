name = "Eat Shit (and die)"
description = "Manure and guano becomes eatable along with plenty of other user-configured options."
author = "UndeadRyker"
version = "1.1.1"
forumthread = ""
api_version = 10
all_clients_require_mod = false
client_only_mod = false
dst_compatible = true
dont_starve_compatible = false
reign_of_giants_compatible = false
shipwrecked_compatible = false
standalone = false
server_filter_tags = {
	"poop", "shit"
}
icon = "modicon.tex"
icon_atlas = "modicon.xml"
configuration_options =
{
	{
		name = "est_gobatshit",
		label = "Batshit mode",
		hover = "Insanity auras for shit.\n(Not meant to be balanced)",
		options =
		{
			{description="Enabled",data=true},
			{description="Disabled",data=false},
		},
		default = false,
	},
	{
		name = "est_notlikerot",
		label = "Positive hunger values",
		hover = "Eating shit gives hunger.\n(This inverts the Don't Starve spoiled food behavior for shit.)",
		options =
		{
			{description="Yes",data=true},
			{description="No",data=false},
		},
		default = false,
	},
	{
		name = "est_shiteatingwigfrid",
		label = "Shit-eating Wigfrid",
		hover = "Enable Wigfrid to eat shit.\n(Also allows monsters and animals to eat shit.)",
		options =
		{
			{description="On",data=true},
			{description="Off",data=false},
		},
		default = false,
	},
	{
		name = "est_eatbucketofshit",
		label = "Eatable Bucket-o-shit",
		hover = "Allows consumption of contents inside the Bucket-o-poop.",
		options =
		{
			{description="On",data=true},
			{description="Off",data=false},
		},
		default = false,
	},
	{
		name = "est_jokelines",
		label = "Alternative inspect lines",
		hover = "New inspect lines for our heroes.\nWARNING: May not work properly with mods that add new characters.",
		options =
		{
			{description="On",data=true},
			{description="Off",data=false},
		},
		default = true,
	},

}